#[rustfmt::skip]
const DEC_DIGITS2: [[u8; 2]; 100] = [
    *b"00", *b"01", *b"02", *b"03", *b"04", *b"05", *b"06", *b"07", *b"08", *b"09",
    *b"10", *b"11", *b"12", *b"13", *b"14", *b"15", *b"16", *b"17", *b"18", *b"19",
    *b"20", *b"21", *b"22", *b"23", *b"24", *b"25", *b"26", *b"27", *b"28", *b"29",
    *b"30", *b"31", *b"32", *b"33", *b"34", *b"35", *b"36", *b"37", *b"38", *b"39",
    *b"40", *b"41", *b"42", *b"43", *b"44", *b"45", *b"46", *b"47", *b"48", *b"49",
    *b"50", *b"51", *b"52", *b"53", *b"54", *b"55", *b"56", *b"57", *b"58", *b"59",
    *b"60", *b"61", *b"62", *b"63", *b"64", *b"65", *b"66", *b"67", *b"68", *b"69",
    *b"70", *b"71", *b"72", *b"73", *b"74", *b"75", *b"76", *b"77", *b"78", *b"79",
    *b"80", *b"81", *b"82", *b"83", *b"84", *b"85", *b"86", *b"87", *b"88", *b"89",
    *b"90", *b"91", *b"92", *b"93", *b"94", *b"95", *b"96", *b"97", *b"98", *b"99",
];
#[inline]
fn byte2dec_i(n: u8, x: &mut [u8; 3]) -> usize {
    if n >= 100 {
        let di = n % 100;
        let n = n / 100;
        let d = DEC_DIGITS2[di as usize];
        x[0] = n + b'0';
        x[1] = d[0];
        x[2] = d[1];
        3
    } else if n >= 10 {
        let d = DEC_DIGITS2[n as usize];
        x[0] = d[0];
        x[1] = d[1];
        2
    } else {
        x[0] = (n as u8) + b'0';
        1
    }
}
#[inline]
pub(crate) fn byte2dec(n: u8, x: &mut [u8; 3]) -> &str {
    let i = byte2dec_i(n, x);
    std::str::from_utf8(&x[..i]).unwrap()
}
#[cfg(test)]
#[test]
fn test_byte2dec() {
    for i in 0..=255 {
        let mut buf = [0u8; 3];
        let r = byte2dec_i(i, &mut buf);
        assert_eq!(&buf[..r], format!("{}", i).as_bytes());
    }
}
