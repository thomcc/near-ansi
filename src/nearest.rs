#![allow(clippy::unreadable_literal)]

use super::tables::*;
use super::{ColorSupport, TermColor};
use rustc_hash::FxHashMap;
use std::cell::{Cell, RefCell};
use ColorSupport::*;

/// Finds the closest supported color to your input color.
///
/// This is thehe main workhorse of this crate. You initialize it with
/// [`ColorSupport`] information, and then pass colors to its `get` function,
/// which will internally cache any expensive operations that are needed (mainly
/// color distance).
pub struct ColorApproximator {
    // the supported colors.
    colors: ColorSupport,

    // The range of the lookup tables (from `ANSI256_*`) that we search
    // when looking for a color. Note that for 256-color, we actually avoid
    // using the "named" (lower-16) colors, since they're customizable, and so
    // if we're looking for the closest color to a given RGB code...
    table_range: std::ops::Range<usize>,

    lab_tab: &'static [LabColor],

    // Maps packed TermColors to supported ansi colors.
    cache: RefCell<FxHashMap<u32, u8>>,
    // TODO: would be nice to use lru-cache here but the performance is worse.
    cache_limit: Cell<Option<usize>>,
}

impl ColorApproximator {
    pub fn new(colors: ColorSupport) -> Self {
        let (rgb_tab, lab_tab, range) = match colors {
            Color8 => (&ANSI_256_RGB[..], &ANSI_256_LAB[..], 0..8),
            Color16 => (&ANSI_256_RGB[..], &ANSI_256_LAB[..], 0..16),
            Color256 => (&ANSI_256_RGB[..], &ANSI_256_LAB[..], 16..256),
            Color88 => (&ANSI_88_RGB[..], &ANSI_88_LAB[..], 16..88),
            TrueColor => (&ANSI_256_RGB[..], &ANSI_256_LAB[..], 16..256),
            NoColor => (&ANSI_256_RGB[..], &ANSI_256_LAB[..], 0..0),
        };

        Self {
            cache: RefCell::new(
                range
                    .clone()
                    .map(|i| (pack_rgb(rgb_tab[i].0, rgb_tab[i].1, rgb_tab[i].2), i as u8))
                    .collect(),
            ),
            table_range: range,
            // rgb_tab,
            lab_tab,
            colors,
            cache_limit: Cell::new(Some(4096)),
            // prefer_ansi: true,
        }
    }

    pub fn color_support(&self) -> ColorSupport {
        self.colors
    }

    pub fn clear_cache(&self) {
        self.cache.borrow_mut().clear();
    }

    pub fn set_cache_limit(&mut self, v: Option<usize>) {
        self.cache_limit.set(v);
    }

    /// Find the nearest color to `c` that should be representable given the
    /// `ColorSupport` setting we were initialized with.
    ///
    /// Returns `None` iff our configured ColorSupport value is `None`.
    pub fn get(&self, c: TermColor) -> Option<TermColor> {
        use TermColor::*;

        match (c, self.colors) {
            (_, NoColor) => None,
            (Ansi(_), TrueColor) => Some(c),

            (Rgb(0, 0, 0), TrueColor) | (Rgb(0, 0, 0), Color256) => Some(Ansi(16)),

            (Rgb(0xff, 0xff, 0xff), TrueColor) | (Rgb(0xff, 0xff, 0xff), Color256) => {
                Some(Ansi(231))
            }

            (Rgb(r, g, b), TrueColor) => {
                if !possibly_ansi256_rgb(r, g, b) {
                    Some(c)
                } else {
                    Some(
                        if let Some(n) = self.cache.borrow().get(&pack_rgb(r, g, b)).copied() {
                            Ansi(n)
                        } else {
                            c
                        },
                    )
                }
            }
            (Ansi(_), Color256) => Some(c),
            (Ansi(n), Color88) if n < 88 => Some(c),
            (Ansi(n), Color16) if n < 16 => Some(c),
            (Ansi(n), Color8) if n < 8 => Some(c),

            // Unbrighten "bright" colors if they are unsupported.
            (Ansi(n), Color8) if n < 16 => Some(Ansi(n - 8)),

            // Some other unsupported color...
            (_, Color8) | (_, Color16) | (_, Color256) | (_, Color88) => self.get_or_insert(c),
        }
    }

    fn get_or_insert(&self, tc: TermColor) -> Option<TermColor> {
        use std::collections::hash_map::Entry;

        let tab = self.lab_tab;
        let range = self.table_range.clone();
        let mut cache = self.cache.borrow_mut();
        let len = cache.len();
        let max = self.cache_limit.get().unwrap_or_else(usize::max_value);
        if cache.len() >= max {}
        let result = match cache.entry(pack_term_color(tc)) {
            Entry::Occupied(entry) => *entry.get(),
            Entry::Vacant(entry) => {
                let lab_src = match tc {
                    // Use ANSI256 since it's very unlikely to refer to a
                    // 88-color value, unless it's on a term that supports them
                    // (in which case we wouldn't be here)
                    TermColor::Ansi(n) => ANSI_256_LAB[n as usize],
                    TermColor::Rgb(r, g, b) => LabColor::from_srgb8(r, g, b),
                };
                let code = closest_index(lab_src, tab, range)?;
                debug_assert!(code <= 256);
                if len < max {
                    entry.insert(code as u8);
                }
                code as u8
            }
        };
        Some(TermColor::Ansi(result))
    }
}

fn closest_index(
    src_lab: LabColor,
    tab: &[LabColor],
    range: std::ops::Range<usize>,
) -> Option<usize> {
    let table = &tab[range.clone()];

    let mut best = src_lab.distance(table.get(0)?);
    let mut best_i = 0;

    for (i, col) in table[1..].iter().enumerate() {
        let dist = src_lab.distance(col);
        if dist < best {
            best = dist;
            best_i = i + 1;
        }
    }
    Some(best_i + range.start)
}

#[inline]
fn pack_rgb(r: u8, g: u8, b: u8) -> u32 {
    u32::from_le_bytes([0, b, g, r]) // 0x00rrggbb
}

#[inline]
fn pack_term_color(tc: TermColor) -> u32 {
    match tc {
        TermColor::Ansi(n) => u32::from_le_bytes([0xff, 0, 0, n]),
        TermColor::Rgb(r, g, b) => pack_rgb(r, g, b),
    }
}

/// A color represented in CIE L*a*b* coordinates.
#[derive(Copy, Debug, Clone, PartialEq)]
pub(crate) struct LabColor {
    pub l: f32,
    pub a: f32,
    pub b: f32,
}

impl LabColor {
    pub fn from_srgb8(r: u8, g: u8, b: u8) -> Self {
        // sRGB to linear RGB
        let lr = SRGB_TO_LINEAR[r as usize];
        let lg = SRGB_TO_LINEAR[g as usize];
        let lb = SRGB_TO_LINEAR[b as usize];
        // convert to xyz space (normalize with D65)
        let cx = (lr * 0.4124 + lg * 0.3576 + lb * 0.1805) / 0.95047;
        let cy = lr * 0.2126 + lg * 0.7152 + lb * 0.0722;
        let cz = (lr * 0.0193 + lg * 0.1192 + lb * 0.9505) / 1.08883;

        fn pivot(c: f32) -> f32 {
            if c > 0.008856 {
                c.cbrt()
            } else {
                (7.787037 * c + 16.0) / 116.0
            }
        }

        let fx = pivot(cx);
        let fy = pivot(cy);
        let fz = pivot(cz);

        Self {
            l: 116.0 * fy - 16.0,
            a: 500.0 * (fx - fy),
            b: 200.0 * (fy - fz),
        }
    }

    /// Uses 1974 ΔEab because it's easy and we don't actually care about the
    /// result value's characteristics
    #[inline]
    pub fn distance(self, o: &LabColor) -> f32 {
        let dl = o.l - self.l;
        let da = o.a - self.a;
        let db = o.b - self.b;
        (dl * dl + da * da + db * db).sqrt()
    }
}
impl From<(u8, u8, u8)> for LabColor {
    fn from(v: (u8, u8, u8)) -> Self {
        Self::from_srgb8(v.0, v.1, v.2)
    }
}
