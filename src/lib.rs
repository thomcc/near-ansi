mod color;
mod nearest;
mod support;
mod tables;

pub use color::TermColor;
pub use nearest::ColorApproximator;
pub use support::ColorSupport;
mod util;
