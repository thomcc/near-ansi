/// A `TermColor` represents the possible ways colors may be displayed on a
/// terminal.
///
/// It's used both as input and output for the `ColorApproximator`, the
/// input-colors will be whatever you happen to be comortable working with
/// internally.
///
/// # Parsing
///
/// TermColors can be parsed from strings using FromStr. A small number of
/// methods of reading them are supported:
///
/// - Hex colors starting with pound, e.g. `#ff3300`
///     - As an extension, numbers of the form `#NN000000`, (4-components, with
///       only the first used) will be treated as a TermColor::Ansi with that
///       code. (This format was developed by `bat` for it's configuration, and
///       I think it's fairly clever). For example: `#07000000` is 7 (white).
///
///     - This is what `Display`ing a `TermColor` uses (and also serialization
///       if serde support is enabled).
///
/// - A numeric literal color will be assumed to be an ansi code, e.g. `15`,
///   `0x27`, or `232`.
///
/// - One of the following names, case insensitive: "black", "red", "green",
///   "yellow", "blue", "magenta", "cyan", "white", will be taken to refer to
///   the first 8 ansi colors.
///
///     - They can be prefixed with "bright" in some manner to refer to the
///       brightened versions, for example `BrightGreen`, `bright green` and
///       `bright_green` all work.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TermColor {
    Ansi(u8),
    Rgb(u8, u8, u8),
}

impl TermColor {
    /// Write ansi escapes directly to the provided buffer. (Note that there are
    /// valid reasons to criticise this -- applications might want to use a
    /// terminfo aware approach, but for a lot fo things it's likely to be
    /// fine).
    pub fn write_ansi(self, buf: &mut String, fg: bool) {
        const ANSI_ESCAPES: [[&str; 2]; 8] = [
            ["\x1b[30m", "\x1b[40m"], // black
            ["\x1b[31m", "\x1b[41m"], // red
            ["\x1b[32m", "\x1b[42m"], // green
            ["\x1b[33m", "\x1b[43m"], // yellow
            ["\x1b[34m", "\x1b[44m"], // blue
            ["\x1b[35m", "\x1b[45m"], // magenta
            ["\x1b[36m", "\x1b[46m"], // cyan
            ["\x1b[37m", "\x1b[47m"], // white
        ];
        match self {
            Self::Ansi(n) if (n as usize) < ANSI_ESCAPES.len() => {
                buf.push_str(ANSI_ESCAPES[n as usize][(!fg) as usize]);
            }
            Self::Ansi(n) => {
                buf.push_str(if fg { "\x1b[38;5;" } else { "\x1b[48;5;" });
                buf.push_str(crate::util::byte2dec(n, &mut [0u8; 3]));
                buf.push('m');
            }
            Self::Rgb(r, g, b) => {
                buf.push_str(if fg { "\x1b[38;2;" } else { "\x1b[48;2;" });
                buf.push_str(crate::util::byte2dec(r, &mut [0u8; 3]));
                buf.push(';');
                buf.push_str(crate::util::byte2dec(g, &mut [0u8; 3]));
                buf.push(';');
                buf.push_str(crate::util::byte2dec(b, &mut [0u8; 3]));
                buf.push('m');
            }
        }
    }
}

impl std::fmt::Display for TermColor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Ansi(v) => write!(f, "#{:02x}000000", v),
            Self::Rgb(r, g, b) => write!(f, "#{:02x}{:02x}{:02x}ff", r, g, b),
        }
    }
}

impl std::str::FromStr for TermColor {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ParseErrKind::*;
        let ts = s.trim();
        if ts.is_empty() {
            return Err(err(s, EmptyStr));
        }

        let sb = ts.as_bytes();
        if sb[0] == b'#' {
            let hex = &sb[1..];
            let normed = match hex.len() {
                3 => [hex[0], hex[0], hex[1], hex[1], hex[2], hex[2], b'f', b'f'],
                6 => [hex[0], hex[1], hex[2], hex[3], hex[4], hex[5], b'f', b'f'],
                8 => [
                    hex[0], hex[1], hex[2], hex[3], hex[4], hex[5], hex[6], hex[8],
                ],
                _ => return Err(err(s, InvalidHexColorLen)),
            };
            let mut rgba = [0u8; 4];
            if base16::decode_slice(&normed, &mut rgba).is_err() {
                return Err(err(s, InvalidHexColor));
            }
            // `bat`-tmTheme compatibility: #RR000000 means to interpret `RR` as
            // an ANSI code.
            return if rgba[3] == 0 && rgba[2] == 0 && rgba[1] == 0 {
                Ok(TermColor::Ansi(rgba[0]))
            } else {
                Ok(TermColor::Rgb(rgba[0], rgba[1], rgba[2]))
            };
        }
        if let Ok(n) = ts.parse::<u8>() {
            return Ok(TermColor::Ansi(n));
        } else if ts.starts_with("0x") || ts.starts_with("0X") {
            return u8::from_str_radix(&ts[2..], 16)
                .map(TermColor::Ansi)
                .map_err(|_| err(s, BadNumberLiteral));
        }

        const NAMES: &[&str] = &[
            "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white",
        ];
        if let Some(t) = NAMES
            .iter()
            .enumerate()
            .find(|t| ts.eq_ignore_ascii_case(t.1))
        {
            return Ok(TermColor::Ansi(t.0 as u8));
        }
        let ts = ts.to_ascii_lowercase();
        if ts.starts_with("bright") {
            if let Some((i, _)) = NAMES.iter().enumerate().find(|t| ts.ends_with(t.1)) {
                return Ok(TermColor::Ansi(i as u8 + 8));
            }
        }

        Err(err(s, UnknownFormat))
    }
}

/// Indicates the reason why TermColor::from_str failed
#[derive(Debug, Clone, PartialEq)]
pub enum ParseErrKind {
    EmptyStr,
    InvalidHexColorLen,
    InvalidHexColor,
    BadNumberLiteral,
    UnknownFormat,

    #[doc(hidden)]
    __Nonexhaustive,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ParseError(Box<(String, ParseErrKind)>);

impl ParseError {
    pub fn source_text(&self) -> &str {
        &*(self.0).0
    }
    pub fn kind(&self) -> &ParseErrKind {
        &(self.0).1
    }
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed to parse color string {:?}: ", self.source_text())?;
        use ParseErrKind::*;
        match self.kind() {
            EmptyStr => f.write_str("an empty string is not valid"),
            InvalidHexColorLen => f.write_str("hex colors should have 3, 6, or 8 hex characters"),
            InvalidHexColor => f.write_str("hex color contained non-hex text"),
            BadNumberLiteral => f.write_str("Failed to parse numeric color literal"),
            UnknownFormat => f.write_str(
                "color was in an unsupported format, or referred to an unfamiliar color name",
            ),
            __Nonexhaustive => Ok(()),
        }
    }
}

impl std::error::Error for ParseError {}

#[cold]
fn err(s: &str, e: ParseErrKind) -> ParseError {
    ParseError(Box::new((s.to_owned(), e)))
}
#[cfg(feature = "serde_support")]
mod serde_impl {
    use super::*;
    impl serde::Serialize for TermColor {
        fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
            s.collect_str(self)
        }
    }

    impl<'de> serde::Deserialize<'de> for TermColor {
        fn deserialize<D: serde::Deserializer<'de>>(de: D) -> Result<Self, D::Error> {
            struct Visitor;
            impl<'de> serde::de::Visitor<'de> for Visitor {
                type Value = TermColor;
                fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    f.write_str("a term color string")
                }
                fn visit_str<E: serde::de::Error>(self, v: &str) -> Result<Self::Value, E> {
                    v.parse::<TermColor>()
                        .map_err(|e| serde::de::Error::custom(e.to_string()))
                }
            }
            de.deserialize_str(Visitor)
        }
    }
}
