/// Enum of how many colors a terminal might support (that we also understand
/// how to write).
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ColorSupport {
    /// Color support disabled.
    NoColor,

    /// The 8 standard ansi colors. Lowest common denominator for things that
    /// have color support.
    ///
    /// Note that these are typically customized to be different colors than the
    /// standard would have you believe in nicer terminals, so we avoid using
    /// them as fallbacks unless the terminal only doesn't support many colors.
    Color8,

    /// The 8 ansi colors, and then "bright" versions.
    Color16,

    /// `xterm-88color`, `rxvt-88color`...
    Color88,

    /// 8-bit (256-color) support.
    Color256,

    /// True (24-bit) color, the gold standard.
    TrueColor,
}

impl ColorSupport {
    pub fn autodetect() -> Self {
        let mut term = std::env::var("TERM").unwrap_or_default();
        term.make_ascii_lowercase();
        if term == "dumb" || std::env::var_os("NO_COLOR").is_some() {
            return ColorSupport::NoColor;
        }

        // if `STY` is set, we're in `screen`, which probably will inherit e.g.
        // `COLORTERM`, and same for the emacs terminal if emacs was launched
        // from the CLI.
        let ignore_colorterm = std::env::var_os("STY").is_some() || term.starts_with("eterm");

        if !ignore_colorterm {
            let ct = std::env::var("COLORTERM").ok();
            let true_color = ct.map_or(false, |t| {
                t.eq_ignore_ascii_case("truecolor")
                    || t.eq_ignore_ascii_case("24bit")
                    || t.eq_ignore_ascii_case("24-bit")
            });
            if true_color {
                return ColorSupport::TrueColor;
            }
        }

        if term.contains("truecolor") || term.contains("direct") {
            return ColorSupport::TrueColor;
        }
        // Check 88-color first
        if term.contains("88") {
            return ColorSupport::Color88;
        }

        if term.contains("16") {
            return ColorSupport::Color16;
        }

        if term.contains("256") || term.contains("xterm") {
            // In practice every xterm that you can find these days the only
            // real exception AFAICT is old versions of apple terminal, but this
            // was fixed in 10.7, which is quite old now.
            return ColorSupport::Color256;
        }

        let fallback = if term.contains("color") {
            ColorSupport::Color8
        } else {
            ColorSupport::NoColor
        };

        query_tput().unwrap_or(fallback)
    }

    #[inline]
    pub fn count(self) -> usize {
        match self {
            ColorSupport::TrueColor => 0x1_00_00_00,
            ColorSupport::Color256 => 256,
            ColorSupport::Color8 => 8,
            ColorSupport::Color16 => 16,
            ColorSupport::Color88 => 88,
            ColorSupport::NoColor => 0,
        }
    }
}

impl std::str::FromStr for ColorSupport {
    type Err = ();
    fn from_str(src: &str) -> Result<Self, Self::Err> {
        if src.eq_ignore_ascii_case("truecolor") {
            Ok(Self::TrueColor)
        } else if src.contains("256") {
            Ok(Self::Color256)
        } else if src.contains("88") {
            Ok(Self::Color88)
        } else if src.contains("16") {
            Ok(Self::Color16)
        } else if src.contains('8') {
            Ok(Self::Color8)
        } else if src == "" || src == "0" || src.eq_ignore_ascii_case("nocolor") {
            Ok(Self::NoColor)
        } else {
            Err(())
        }
    }
}

impl std::fmt::Display for ColorSupport {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Self::NoColor => "nocolor",
            Self::Color8 => "color8",
            Self::Color16 => "color16",
            Self::Color88 => "color88",
            Self::Color256 => "color256",
            Self::TrueColor => "truecolor",
        })
    }
}

fn query_tput() -> Option<ColorSupport> {
    // New terminfo dbs for truecolor terms report the RGB boolean capability.
    // This is still pretty rare in practice though, and not worth it yet...

    // if tput("RGB").is_some() {
    //     return Some(ColorSupport::TrueColor);
    // }

    // Otherwise, check the numeric `colors` capability.
    let output = tput("colors")?;

    let n = if output.starts_with("0x") || output.starts_with("0X") {
        i32::from_str_radix(&output[2..], 16).ok()
    } else {
        output.parse::<i32>().ok()
    }?;
    let requirements = [
        (0x1_00_00_00, ColorSupport::TrueColor),
        (256, ColorSupport::Color256),
        (88, ColorSupport::Color88),
        (16, ColorSupport::Color16),
        (8, ColorSupport::Color8),
    ];
    Some(
        requirements
            .iter()
            .find(|&&(r, _)| r >= n)
            .map(|&(_, c)| c)
            .unwrap_or(ColorSupport::NoColor),
    )
}

fn tput(req: &str) -> Option<String> {
    let output = std::process::Command::new("tput").arg(req).output().ok()?;
    if !output.status.success() {
        None
    } else {
        Some(String::from_utf8_lossy(&output.stdout).to_string())
    }
}

#[cfg(feature = "serde_support")]
mod serde_impl {
    use super::*;
    impl serde::Serialize for ColorSupport {
        fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
            s.collect_str(self)
        }
    }

    impl<'de> serde::Deserialize<'de> for ColorSupport {
        fn deserialize<D: serde::Deserializer<'de>>(de: D) -> Result<Self, D::Error> {
            struct Visitor;
            impl<'de> serde::de::Visitor<'de> for Visitor {
                type Value = ColorSupport;
                fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    f.write_str("a color support string")
                }
                fn visit_str<E: serde::de::Error>(self, v: &str) -> Result<Self::Value, E> {
                    v.parse::<ColorSupport>()
                        .map_err(|_| serde::de::Error::custom(format!("invalid string: {:?}", v)))
                }
            }
            de.deserialize_str(Visitor)
        }
    }
}
