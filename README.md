# near-ansi

A small rust crate for approximating (24-bit) to some color format supported by
the in-use terminal.

At the moment it's about 90% done -- mostly I've convinced myself it's not
necessary for my use case, but will probably finish it up soon.

## Features

1. Automatically detecting the terminal's color support, but it can be manually
   specified as well.

2. Going from 24-bit RGB to whatever is supported out of 256-color, 88-color,
   16-color, or 8-color. (It can actually go from 256 to nearest in 88, 16, 8 as
   well, or more generally "any wider" to "any narrower").

3. Approximation happens in CIE L\*a\*b\* space (e.g. it's actually a correct
   approximation) and is automatically cached.

4. Fairly fast (I'm unaware of anything similar that's faster without abandoning
   correctness).
    - Can optionally switch out a (expensive-to-output) RGB color for the
      identical ANSI color code if one exists, even in the case where the
      underlying terminal supports truecolor, which can improve performance
      more.

5. Parsing and printing colors in a manner consistent with various other
   libraries (including extensions invented by e.g. `bat`, e.g. #XY000000 for
   the color with ansi code 0xXY).

6. Producing the relevant ANSI code to enable a given color (foreground or
   background).

## TODO

1. Usage examples, more testing.
2. Improve library ergonomics for non-advanced cases.
3. Support an uncached usage mode (caching happens internally, as in many cases
   we can avoid anything costly that requires accessing the cache).
4. Better support for cache limits. Right now the support is pretty hacky, and
   so if you approximate a large number of colors, you might have issues. It's
   mostly intended for cases like applying a theme for syntax highlighting,
   where the set of colors being approximated is closed.
5. Consider things like detecting (lack of) bold/dim support and replacing it
   with color change.
6. When writing out an ansi/rgb code (e.g. feature #6) follow what terminfo
   suggests.
